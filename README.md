## BLOCKS BOT
### Simple bot to announce to slack/telegram when block is found in xmrpool.eu

Requirements
-------------
python 3 and above. _Will probably run in python2.x but I have not bothered._
That's it

Intro
-----

At a minumum all you need to do is configure a json config file with required parameters.
Copy sample-config.json file included to bot-config.json and modify the required parameters.

Configuration
-----------------
The configuration is in simple json format.
The following keys are available:

**BLOCK_CONF**
A JSON file that stores found blocks history.
This path can be any file but must be writable by this bot.

**STATS_URL**
The pool stats API url ofcourse.

Default: `https://web.xmrpool.eu:8119/stats`

**SLACK_URL**
Slack webhook url (xmrpooleu.slack.com)
You can create this in the slack channel once logged in.
Navigate to [Add incoming-webhooks](https://xmrpooleu.slack.com/apps/new/A0F7XDUAZ-incoming-webhooks)

**SLACK_CHANNELS**
All the Slack channels that the bot will post to when a block is found.

Default: `["#blocks", "#general"]`

**ALT_API**
An array collection of all external api's for fetching Block info.
(useful for getting block reward during maturity).

Default:
```javascript
"ALT_API": [
        {
            "name": "xmrchain.net",
            "api_url": "https://xmrchain.net/api/block",
            "reward_key": "data.txs.0.xmr_outputs",
            "success_key": "status",
            "block_url": "https://xmrchain.net/block"
        },
        {
            "name": "moneroexplorer.pro",
            "api_url": "https://moneroexplorer.pro/api/block",
            "reward_key": "data.txs.0.xmr_outputs",
            "success_key": "status",
            "block_url": "https://moneroexplorer.pro/block"
        },
        {
            "name": "moneroblocks.info",
            "api_url": "https://moneroblocks.info/api/get_block_header",
            "reward_key": "block_header.reward",
            "success_key": "status",
            "block_url": "https://moneroblocks.info/block"
        }
    ]

```

Usage
-----
To run the bot:

- Open terminal/command shell window in the parent folder _(which is the folder with blocks-bot.py)_
- Copy the sample config file sample-config.json to bot-config.json
- Edit bot-config.json and add the proper values for **BLOCK_CONF**, **SLACK_URL** and **SLACK_CHANNELS**
- Run the command:

_Assuming python is in your environment path:_
```sh
 python3 blocks-bot.py
```

The ideal scenario is to run the setup as a cronjob in linux/mac or scheduled tasks in windows to periodically call this python script.

_(TODO: Integrate with telegram)_

