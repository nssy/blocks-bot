#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: NssY Wanyonyi
# Purpose: Check pool for new blocks and report to slack/telegram
# Created: 26-June-2018
# Modified: 27-January-2021

import requests
import sys
import os
import json
from datetime import datetime

DIR = os.path.dirname(os.path.realpath(__file__))

# Bot settings stored in json file
CONFIG_FILE = os.path.join(DIR, 'bot-config.json')

# populate config dictionary with all config values
# from our json file
with open(CONFIG_FILE, 'r') as f:
    config = json.load(f)


def main():
    ''''''

    foundBlocks = {}
    blocksHistory = {'lastBlockFound': 0, 'blocks': {}}

    # Sanity check (make sure our config file exists)
    if not os.path.isfile(config['BLOCK_CONF']):
        print(' -> Missing config file {0}'.format(config['BLOCK_CONF']))

        # Try to create the config file
        try:
            with open(config['BLOCK_CONF'], 'w') as b:
                json.dump(blocksHistory, b)
        except Exception as e:
            print(' -> Unable to create the config file {0}'.format(
                config['BLOCK_CONF']))
    else:
        # Get last block find time from config file
        try:
            if os.access(config['BLOCK_CONF'], os.R_OK):
                with open(config['BLOCK_CONF'], 'r') as f:
                    b = json.load(f)

                    if set(("lastBlockFound", "blocks")) <= set(b):
                        blocksHistory = b
        except Exception as e:
            # Update config file
            if os.access(config['BLOCK_CONF'], os.W_OK):
                with open(config['BLOCK_CONF'], 'w') as b:
                    json.dump(blocksHistory, b)
            else:
                print(' -> Unable to write to config file {0}'.format(
                    config['BLOCK_CONF']))

    requests.packages.urllib3.disable_warnings()

    # All the bot magic is here
    # Get Stats
    try:
        response = requests.get(
            config['STATS_URL'],
            timeout=10,
            verify=False)

        if response.status_code != 200:
            print(" -> Bad status code: {0}".format(response.status_code))
            print(' -> continuing ... ')
            return 1

        pool_stats = response.json()

        # Get the top most block find time
        s = int(pool_stats['pool']['stats']['lastBlockFound'][:10])

        if (s > int(blocksHistory['lastBlockFound'])):
            # We have a new block
            blocks = {}

            # Loop through all the blocks into a dictionary
            # Note: This is the most effective way when we have
            # multiple blocks maturing
            for i in range(int(len(pool_stats['pool']['blocks'])/2)):
                b_string, height = pool_stats['pool']['blocks'][:2]
                del pool_stats['pool']['blocks'][:2]

                b_data = b_string.split(':')
                time = int(b_data[1])

                blocks[time] = {}
                blocks[time]['height'] = height
                blocks[time]['hash'] = b_data[0]
                blocks[time]['time'] = time
                blocks[time]['diff'] = int(b_data[2])
                blocks[time]['shares'] = int(b_data[3])
                blocks[time]['block_url'] = 'https://xmrchain.net/block/{0}'.format(b_data[0])

                try:
                    blocks[time]['orphan'] = b_data[4]
                except Exception as e:
                    blocks[time]['orphan'] = None
                try:
                    blocks[time]['reward'] = int(b_data[5])/1000000000000.0
                    blocks[time]['status'] = True
                    blocks[time]['verifier'] = ''  # Pool
                except Exception as e:
                    blocks[time]['reward'] = -1
                    # status is false until we successfully get block reward
                    blocks[time]['status'] = False
                    blocks[time]['verifier'] = ''

                # if difference is small
                # We can be sure this is the one
                # Note: This is necessary since api
                # value "lastBlockFound" sometimes varies from
                # what is in blocks json
                if abs(time - s) < 300:
                    #  Compare height of previously announced blocks
                    if height not in blocksHistory['blocks'].keys():
                        foundBlocks[height] = blocks[time]

            if len(foundBlocks) == 0:
                return 0

            for i, block in sorted(foundBlocks.items()):
                if not set(("hash", "reward")) <= set(block):
                    continue

                # Countercheck block with ALT API's if no reward available
                block = counter_check(block)

                # log to console
                print(" -> Found: {0} | {1:,} | {2:,} | {3} | {4} | {5}".format(
                    block['height'],
                    block['shares'],
                    block['diff'],
                    datetime.fromtimestamp(int(block['time'])).strftime('%F %H:%M:%S'),
                    block['reward'] if block['reward'] > 0 else block['error'],
                    block['verifier']))

                # Notify on slack
                to_slack(block)

                blocksHistory['lastBlockFound'] = s

                # we don't need to store block_url, status and verifier
                blocksHistory['blocks'][block['height']] = {key: block[key] for key in block if key not in ['block_url', 'status', 'verifier']}

            # We only want to keep 10 blocks
            if len(blocksHistory['blocks']) > 10:
                delete = set(blocksHistory['blocks'].keys()).difference(sorted(blocksHistory['blocks'].keys())[-10:])
                for d in delete:
                    del blocksHistory['blocks'][d]

            # Update our config file config['BLOCK_CONF']
            if (os.access(config['BLOCK_CONF'], os.W_OK)):
                with open(config['BLOCK_CONF'], 'w') as b:
                    json.dump(blocksHistory, b)
            return 0

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        sys.stderr.write(' -> {} \033[0;31mError: \033[0;33m{} {} {} {}\033[0m\n'.format(
            datetime.now().strftime('%F %H:%M:%S'),
            str(e), type(e), 'line:', exc_tb.tb_lineno))
        print(' -> continuing ... ')
        return 1


def counter_check(block):
    ''' Countercheck block with ALT API's '''

    for api in config['ALT_API']:
        try:
            response = requests.get(
                api['api_url'] + "/" + block['hash'],
                timeout=10,
                verify=False)

            if response.status_code != 200:
                print(" -> Bad status code: {0} ({1})".format(
                    response.status_code,
                    api['name']))
                print(' -> continuing ... ')
                continue

            reward = response.json()
            keys = api['reward_key'].split('.')

            # Improvised iteration of api['reward_key']
            # Works rather well I'm suprised
            for key in keys:
                try:
                    key = int(key)
                except ValueError:
                    pass
                reward = reward[key]

            block['reward'] = int(reward)/1000000000000.0
            block['verifier'] = api['name']
            block['status'] = True
            block['block_url'] = api['block_url'] + "/" + block['hash']
            block['error'] = ''
            break
        except Exception as e:
            block['reward'] = -1
            block['error'] = 'Block not found'
            exc_type, exc_obj, exc_tb = sys.exc_info()
            sys.stderr.write(' -> {} \033[0;31mError: \033[0;33m{} {} {} {}\033[0m\n'.format(
                datetime.now().strftime('%F %H:%M:%S'),
                str(e), type(e), 'line:', exc_tb.tb_lineno))
    return block


def to_slack(block):
    ''' Post to Slack '''

    # Get emojis: https://slackmojis.com/
    payload = {'icon_emoji': ':trumpet:', 'username': 'blocks-bot'}

    effort = round(block['shares'] / block['diff'] * 100, 2)

    # Fancy messages (Make bot more human)
    if (effort < 1):
        msg = '*OMG:* _would you look at that luck! ... _ :heart_eyes: :star-struck: :dancing:'
        b_color = '#045004'
        a_style = 'primary'
        payload['icon_emoji'] = ':tada:'
    elif (effort < 5):
        msg = '_I am speechless! ... _ :dancing:'
        b_color = '#027302'
        a_style = 'primary'
        payload['icon_emoji'] = ':tada:'
    elif (effort < 10):
        msg = '_Wow! ... _ :tada: :star-struck:'
        b_color = '#039403'
        a_style = 'primary'
    elif (effort < 20):
        msg = '_Amazing luck! ... _ :tada: :man_dancing:'
        b_color = '#039403'
        a_style = 'primary'
    elif (effort < 99.5):
        msg = '_Yay its green! ... _ :tada:'
        b_color = '#00c500'
        a_style = 'primary'
    elif (effort >= 99.5 and effort < 101):
        b_color = '#cccccc'
        msg = '_What a rare ocassion !_ (effort: {0}%) :pick:'.format(effort)
        a_style = 'primary'
    elif (effort >= 101 and effort < 200):
        msg = '_Finally! ... _ :pray:'
        b_color = '#fd5e00'
        a_style = 'default'
        payload['icon_emoji'] = ':relieved:'
    elif (effort >= 200 and effort < 400):
        msg = '_Oh boy, what a run! ... _ :confounded:'
        b_color = '#d23c00'
        a_style = 'danger'
        payload['icon_emoji'] = ':confounded:'
    else:
        msg = '_Tough one to crack! ... _ :gem: :confounded:'
        b_color = '#d00000'
        a_style = 'danger'
        payload['icon_emoji'] = ':gem:'

    # Possible orphaned block
    if not block['status']:
        msg += "\n .. :confused: "
        payload['icon_emoji'] = ':thinking_face:'

    if block['reward'] > 0:
        reward = ' | Reward: {0}'.format(round(block['reward'], 3))
    else:
        reward = ''

    payload['attachments'] = [{
        'fallback': '*B L O C K ! ({0})*\n{1} (effort: {2}%) !!'.format(
            block['height'],
            msg,
            effort),
        'color':b_color,
        'title':'B L O C K ! ({0})'.format(block['height']),
        'text': msg,
        'actions': [
            {'type': 'button',
             'name': 'blocks_page',
             'text': 'Pool Blocks',
             'url': 'https://web.xmrpool.eu/xmr-pool-blocks.html',
             'style': a_style},
            {'type': 'button',
             'name': 'explore_{}'.format(block['height']),
             'text': 'Explore {}'.format("@ " + block['verifier'] if block['verifier'] else ''),
             'url': block['block_url'],
             'style': a_style}],
        'footer': 'Shares: {0:,} | Diff: {1:,} | Effort: {2}%{3}'.format(
            block['shares'],
            block['diff'],
            effort,
            reward),
        'footer_icon': 'https://emojis.slackmojis.com/emojis/images/1507190485/2989/monero.png?1507190485',
        'ts': block['time'],
        'mrkdwn_in': ['text']}]

    # Send to slack channels specified
    for channel in config['SLACK_CHANNELS']:
        print(' -> Sending to slack ({0}) ... '.format(channel), end='')

        payload['channel'] = channel
        response = requests.post(
            config['SLACK_URL'],
            data=json.dumps(payload),
            headers={'Content-Type': 'application/json'},
            timeout=30,
            verify=False)
        print(response.status_code, response.reason)

if __name__ == '__main__':
    bot = main()
    sys.exit(bot)
