#!/usr/bin/env bash
# Author: NssY Wanyonyi
# Purpose: Run python blocks-bot.py every N seconds
# Created: 26-June-2018
# Modified: 02-July-2018

# Duration to wait in seconds
DURATION=30

# Main
echo " -> Checking: https://web.xmrpool.eu:8119/stats every $DURATION sec(s) ... "

while [ true ]; do
	./blocks-bot.py
	sleep $DURATION
done

exit 0